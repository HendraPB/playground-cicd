import React from 'react'
import { render } from 'react-dom'
import App from './App'
import getFatcs from "./facts";

getFatcs().then(facts => {
    render(<App facts={facts} />, document.querySelector('#root'))
})